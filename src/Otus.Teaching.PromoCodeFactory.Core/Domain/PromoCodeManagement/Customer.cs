﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public virtual IList<PromoCode> PromoCodes { get; set; }

        public virtual IList<CustomerPreference> CustomerPreferences { get; set; }
    }
}