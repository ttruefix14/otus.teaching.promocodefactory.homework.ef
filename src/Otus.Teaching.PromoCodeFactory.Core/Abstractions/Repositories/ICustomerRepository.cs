﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        new Task<bool> UpdateAsync(Customer updatedEntity, CancellationToken cancellationToken);

        Task<IEnumerable<Customer>> GetByPreferenceAsync(string PreferenceName, CancellationToken cancellationToken);
    }
}
