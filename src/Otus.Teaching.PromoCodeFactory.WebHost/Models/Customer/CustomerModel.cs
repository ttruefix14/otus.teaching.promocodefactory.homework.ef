﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models.Preference;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.PromoCode;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Customer
{
    public class CustomerModel
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }
        //TODO: Добавить список предпочтений

        public List<PromoCodeShortModel> PromoCodes { get; set; }

        public List<PreferenceModel> Preferences { get; set; }
    }
}