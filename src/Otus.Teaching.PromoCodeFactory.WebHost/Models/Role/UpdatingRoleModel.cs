﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Role
{
    public class UpdatingRoleModel
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}