﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models.Role;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Employee
{
    public class EmployeeModel
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public RoleModel Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}