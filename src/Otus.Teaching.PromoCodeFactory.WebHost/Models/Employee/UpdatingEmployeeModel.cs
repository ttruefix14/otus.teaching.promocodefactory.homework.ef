﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models.Role;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Employee
{
    public class UpdatingEmployeeModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public RoleModel Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}