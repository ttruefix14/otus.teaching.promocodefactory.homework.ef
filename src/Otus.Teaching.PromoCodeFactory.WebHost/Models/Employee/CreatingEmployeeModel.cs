﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Employee
{
    public class CreatingEmployeeModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string RoleName { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}