﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Preference
{
    public class PreferenceModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
