using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Mapping;
using System;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;


namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class Startup
    {
        public Startup(IConfiguration configuration) 
        { 
            Configuration = configuration;
        }
        private IConfiguration Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            InstallAutomapper(services);
            services.AddServices(Configuration);
            services.AddControllers();

            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory API Doc";
                options.Version = "1.0";
            });
            services.AddCors();
        }



        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            if (env.IsDevelopment())
            {
                dbInitializer.InitializeDb();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });
            
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }

        private static IServiceCollection InstallAutomapper(IServiceCollection services)
        {
            services.AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));
            return services;
        }

        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<EmployeeMappingsProfile>();
                cfg.AddProfile<Otus.Teaching.PromoCodeFactory.DataAccess.Services.Implementations.Mapping.EmployeeMappingsProfile>();
                cfg.AddProfile<RoleMappingsProfile>();
                cfg.AddProfile<Otus.Teaching.PromoCodeFactory.DataAccess.Services.Implementations.Mapping.RoleMappingsProfile>();
                cfg.AddProfile<CustomerMappingsProfile>();
                cfg.AddProfile<Otus.Teaching.PromoCodeFactory.DataAccess.Services.Implementations.Mapping.CustomerMappingsProfile>();
                cfg.AddProfile<PreferenceMappingsProfile>();
                cfg.AddProfile<Otus.Teaching.PromoCodeFactory.DataAccess.Services.Implementations.Mapping.PreferenceMappingsProfile>();
                cfg.AddProfile<PromoCodeMappingsProfile>();
                cfg.AddProfile<Otus.Teaching.PromoCodeFactory.DataAccess.Services.Implementations.Mapping.PromoCodeMappingsProfile>();
            });
            configuration.AssertConfigurationIsValid();
            return configuration;
        }
    }
}