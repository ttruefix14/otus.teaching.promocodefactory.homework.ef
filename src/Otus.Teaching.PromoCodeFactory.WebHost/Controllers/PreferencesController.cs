﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Preference;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Preference;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PreferencesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IPreferenceService _preferenceService;

        public PreferencesController(IMapper mapper, IPreferenceService preferenceService)
        {
            _mapper = mapper;
            _preferenceService = preferenceService;
        }
        
        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        // GET: api/<PreferencesController>
        [HttpGet]
        public async Task<ActionResult<PreferenceModel>> GetPreferencesAsync()
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(10));

            return Ok(_mapper.Map<List<PreferenceModel>>(
                await _preferenceService.GetAllAsync(cancellationTokenSource.Token)));
        }

        /// <summary>
        /// Получить данные предпочтения по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/<PreferencesController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PreferenceModel>> GetPreferenceAsync(Guid id)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(10));

            var preference = await _preferenceService.GetByIdAsync(id, cancellationTokenSource.Token);
            if (preference == null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<PreferenceModel>(preference));
        }

        /// <summary>
        /// Создание нового предпочтения
        /// </summary>
        /// <param name="creatingPreferenceModel"></param>
        /// <returns></returns>
        // POST api/<PreferencesController>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreatePreferenceAsync(CreatingPreferenceModel creatingPreferenceModel)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(10));

            return Ok(await _preferenceService.CreateAsync(
                _mapper.Map<CreatingPreferenceDto>(creatingPreferenceModel),
                cancellationTokenSource.Token));
        }

        /// <summary>
        /// Удалить предпочтение
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE api/<PreferencesController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> DeletePreferenceAsync(Guid id)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(10));

            return Ok(await _preferenceService.DeleteAsync(id, cancellationTokenSource.Token));
        }
    }
}
