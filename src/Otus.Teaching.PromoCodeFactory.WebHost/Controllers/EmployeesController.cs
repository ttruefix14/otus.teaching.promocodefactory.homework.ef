﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Employee;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Employee;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IEmployeeService _employeeService;
        private readonly IMapper _mapper;

        public EmployeesController(IEmployeeService employeeService, IMapper mapper)
        {
            _employeeService = employeeService;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<EmployeeModel>>> GetEmployeesAsync()
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(10));

            var employees = await _employeeService.GetAllAsync(cancellationTokenSource.Token);
            return Ok(_mapper.Map<List<EmployeeModel>>(employees));
        }
        
        /// <summary>
        /// Получить данные сотрудника по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeModel>> GetEmployeeByIdAsync(Guid id)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(10));

            var employee = await _employeeService.GetByIdAsync(id, cancellationTokenSource.Token);

            if (employee == null)
                return NotFound();

            return Ok(_mapper.Map<EmployeeModel>(employee));
        }

        /// <summary>
        /// Создать сотрудника и роль сотрудника
        /// </summary>
        /// <param name="employeeModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateEmployeeAsync(CreatingEmployeeModel employeeModel)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(10));

            var guid = await _employeeService.CreateAsync(
                _mapper.Map<CreatingEmployeeDto>(employeeModel), 
                cancellationTokenSource.Token);  
            return Ok(guid);
        }

        /// <summary>
        /// Удалить данные о сотруднике
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> DeleteAsync(Guid id)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(10));

            return Ok(await _employeeService.DeleteAsync(id, cancellationTokenSource.Token));
        }

        /// <summary>
        /// Обновить данные о сотруднике
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updatingEmployeeModel"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<ActionResult<bool>> UpdateAsync(Guid id, UpdatingEmployeeModel updatingEmployeeModel)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(10));

            return Ok(await _employeeService.UpdateAsync(
                id,
                _mapper.Map<UpdatingEmployeeDto>(updatingEmployeeModel),
                cancellationTokenSource.Token));
        }
    }
}