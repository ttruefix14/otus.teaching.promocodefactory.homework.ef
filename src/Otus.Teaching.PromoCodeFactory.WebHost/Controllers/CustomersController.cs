﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Customer;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Customer;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly ICustomerService _customerService;
        private readonly IMapper _mapper;

        public CustomersController(ICustomerService customerService, IMapper mapper)
        {
            _customerService = customerService;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить список клиентов с промокодами и предпочтениями
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerModel>> GetCustomersAsync()
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(10));

            var customers = await _customerService.GetAllAsync(cancellationTokenSource.Token);
            return Ok(_mapper.Map<List<CustomerModel>>(customers));
        }
        
        /// <summary>
        /// Получить данные о клиенте по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerModel>> GetCustomerAsync(Guid id)
        {
            //TODO: Добавить получение клиента вместе с выданными ему промомкодами
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(10));

            var customer = await _customerService.GetByIdAsync(id, cancellationTokenSource.Token);
            if (customer == null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<CustomerModel>(customer));
        }
        
        /// <summary>
        /// Создать клиента со списком предпочтений
        /// </summary>
        /// <param name="creatingCustomerModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreatingOrUpdatingCustomerModel creatingCustomerModel)
        {
            //TODO: Добавить создание нового клиента вместе с его предпочтениями
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(10));

            return Ok(await _customerService.CreateAsync(
                _mapper.Map<CreatingOrUpdatingCustomerDto>(creatingCustomerModel), 
                cancellationTokenSource.Token));
        }
        
        /// <summary>
        /// Обновить данные о клиенте
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updatingCustomerModel"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreatingOrUpdatingCustomerModel updatingCustomerModel)
        {
            //TODO: Обновить данные клиента вместе с его предпочтениями
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(10));

            return Ok(await _customerService.UpdateAsync(
                id, 
                _mapper.Map<CreatingOrUpdatingCustomerDto>(updatingCustomerModel), 
                cancellationTokenSource.Token));
        }
        
        /// <summary>
        /// Удалить клиента и выданные ему промокоды
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(10));

            return Ok(await _customerService.DeleteAsync(id, cancellationTokenSource.Token));
        }
    }
}