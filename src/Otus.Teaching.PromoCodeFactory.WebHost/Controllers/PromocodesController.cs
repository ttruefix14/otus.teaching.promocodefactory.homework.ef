﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.PromoCode;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.PromoCode;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IPromoCodeService _promoCodeService;
        public PromocodesController(IPromoCodeService promoCodeService, IMapper mapper)
        {
            _mapper = mapper;
            _promoCodeService = promoCodeService;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortModel>>> GetPromoCodesAsync()
        {
            //TODO: Получить все промокоды
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(10));

            return Ok(_mapper.Map<List<PromoCodeShortModel>>(
                await _promoCodeService.GetAllAsync(cancellationTokenSource.Token)));
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> GivePromoCodesToCustomersWithPreferenceAsync(CreatingPromoCodeModel creatingPromoCodeModel)
        {
            //TODO: Создать промокод и выдать его клиентам с указанным предпочтением
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(10));

            return Ok(await _promoCodeService.CreateAsync(
                _mapper.Map<CreatingPromoCodeDto>(creatingPromoCodeModel),
                cancellationTokenSource.Token));
        }

        /// <summary>
        /// Удалить промокод
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> DeletePromoCodeAsync(Guid id)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            cancellationTokenSource.CancelAfter(TimeSpan.FromSeconds(10));

            return Ok(await _promoCodeService.DeleteAsync(id, cancellationTokenSource.Token));
        } 
    }
}