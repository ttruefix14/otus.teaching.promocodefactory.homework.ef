﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Preference;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Preference;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping
{
    public class PreferenceMappingsProfile : Profile
    {
        public PreferenceMappingsProfile()
        {
            CreateMap<PreferenceDto, PreferenceModel>().ReverseMap();

            CreateMap<CreatingPreferenceModel, CreatingPreferenceDto>();
        }
    }
}
