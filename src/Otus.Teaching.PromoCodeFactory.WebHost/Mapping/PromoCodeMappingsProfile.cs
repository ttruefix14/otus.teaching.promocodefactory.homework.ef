﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.PromoCode;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.PromoCode;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping
{
    public class PromoCodeMappingsProfile : Profile
    {
        public PromoCodeMappingsProfile()
        {
            CreateMap<PromoCodeDto, PromoCodeShortModel>()
                .ForMember(d => d.BeginDate, map => map.MapFrom(src => src.BeginDate.ToString()))
                .ForMember(d => d.EndDate, map => map.MapFrom(src => src.EndDate.ToString()));

            CreateMap<CreatingPromoCodeModel, CreatingPromoCodeDto>();
        }
    }
}
