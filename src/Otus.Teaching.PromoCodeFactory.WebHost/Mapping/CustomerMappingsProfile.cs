﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Customer;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Preference;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Customer;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping
{
    public class CustomerMappingsProfile : Profile
    {
        public CustomerMappingsProfile()
        {
            CreateMap<CustomerDto, CustomerModel>();

            CreateMap<CreatingOrUpdatingCustomerModel, CreatingOrUpdatingCustomerDto>()
                .ForMember(d => d.Preferences, map => map.MapFrom(src => src.PreferenceIds));

            CreateMap<Guid, PassingPreferenceIdDto>()
                .ForMember(d => d.Id, map => map.MapFrom(src => src));
        }
    }
}
