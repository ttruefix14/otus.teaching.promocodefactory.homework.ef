﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Role;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Role;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping
{
    public class RoleMappingsProfile : Profile
    {
        public RoleMappingsProfile()
        {
            CreateMap<RoleDto, RoleModel>().ReverseMap();

            CreateMap<UpdatingRoleModel, UpdatingRoleDto>();
        }
    }
}
