﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Employee;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Employee;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping
{
    public class EmployeeMappingsProfile : Profile
    {
        public EmployeeMappingsProfile() 
        {
            CreateMap<EmployeeDto, EmployeeModel>();

            CreateMap<CreatingEmployeeModel, CreatingEmployeeDto>();

            CreateMap<UpdatingEmployeeModel, UpdatingEmployeeDto>();
        }
    }
}
