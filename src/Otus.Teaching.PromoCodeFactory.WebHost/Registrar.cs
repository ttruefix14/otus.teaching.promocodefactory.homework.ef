﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Implementations;
using Otus.Teaching.PromoCodeFactory.WebHost.Settings;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public static class Registrar
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration) 
        {
            var applicationSettings = configuration.Get<ApplicationSettings>();
            services.AddSingleton(applicationSettings)
                    .AddSingleton((IConfigurationRoot)configuration)
                    .InstallServices()
                    .ConfigureContext(applicationSettings.ConnectionString)
                    .InstallRepositories()
                    .AddTransient<IDbInitializer, DbInitializer>();
            return services;
        }
        private static IServiceCollection InstallServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IEmployeeService, EmployeeService>();
            serviceCollection.AddTransient<ICustomerService, CustomerService>();
            serviceCollection.AddTransient<IPreferenceService, PreferenceService>();
            serviceCollection.AddTransient<IPromoCodeService, PromoCodeService>();
            return serviceCollection;
        }

        private static IServiceCollection InstallRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddTransient<IRepository<Customer>, EfRepository<Customer>>()
                .AddTransient<IRepository<Preference>, EfRepository<Preference>>()
                .AddTransient<ICustomerRepository, CustomerRepository>()
                .AddTransient<IRepository<Employee>, EfRepository<Employee>>()
                .AddTransient<IRepository<PromoCode>, EfRepository<PromoCode>>()
                .AddTransient<IRepository<Role>, EfRepository<Role>>();
            return serviceCollection;
        }
    }
}
