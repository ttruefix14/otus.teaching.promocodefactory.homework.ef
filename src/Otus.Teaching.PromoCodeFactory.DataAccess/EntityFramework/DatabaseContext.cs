﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }
        public DbSet<Employee> Employees { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.HasDefaultSchema("otus");

            modelBuilder.Entity<PromoCode>()
                .HasOne(p => p.Preference)
                .WithMany()
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<PromoCode>()
                .HasOne(p => p.Customer)
                .WithMany(c => c.PromoCodes);

            modelBuilder.Entity<PromoCode>()
                .HasOne(p => p.PartnerManager)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<PromoCode>().Property(p => p.CodeText).HasMaxLength(255);
            modelBuilder.Entity<PromoCode>().Property(p => p.ServiceInfo).HasMaxLength(255);
            modelBuilder.Entity<PromoCode>().Property(p => p.PartnerName).HasMaxLength(255);

            modelBuilder.Entity<Preference>().Property(p => p.Name).HasMaxLength(255);

            modelBuilder.Entity<Employee>()
                .HasOne(e => e.Role)
                .WithMany()
                .HasForeignKey(e => e.RoleId)
                .IsRequired(false);

            modelBuilder.Entity<Employee>().Property(e => e.FirstName).HasMaxLength(255);
            modelBuilder.Entity<Employee>().Property(e => e.LastName).HasMaxLength(255);
            modelBuilder.Entity<Employee>().Property(e => e.FullName)
               .HasComputedColumnSql("\"FirstName\" || ' ' || \"LastName\"", stored: true)
               .HasMaxLength(511);
            modelBuilder.Entity<Employee>().Property(e => e.Email).HasMaxLength(255);

            modelBuilder.Entity<Role>().Property(r => r.Name).HasMaxLength(255);
            modelBuilder.Entity<Role>().Property(r => r.Description).HasMaxLength(255);

            modelBuilder.Entity<Customer>().Property(e => e.FirstName).HasMaxLength(255);
            modelBuilder.Entity<Customer>().Property(e => e.LastName).HasMaxLength(255);
            modelBuilder.Entity<Customer>().Property(e => e.FullName)
                .HasComputedColumnSql("\"FirstName\" || ' ' || \"LastName\"", stored: true)
                .HasMaxLength(511);
            modelBuilder.Entity<Customer>().Property(e => e.Email).HasMaxLength(255);

            modelBuilder.Entity<Customer>()
                .HasMany(c => c.PromoCodes)
                .WithOne(p => p.Customer)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CustomerPreference>().HasKey(cp => new { cp.CustomerId, cp.PreferenceId });
            modelBuilder.Entity<CustomerPreference>()
                .HasOne<Customer>(cp => cp.Customer)
                .WithMany(c => c.CustomerPreferences);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
        }
    }
}
