﻿using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.PromoCode;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Abstractions
{
    public interface IPromoCodeService
    {
        Task<IEnumerable<PromoCodeDto>> GetAllAsync(CancellationToken cancellationToken);

        Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken);

        Task<IEnumerable<Guid>> CreateAsync(CreatingPromoCodeDto creatingPromoCodeDto, CancellationToken cancellationToken);
    }
}
