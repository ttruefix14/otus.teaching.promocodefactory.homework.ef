﻿using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Preference;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Abstractions
{
    public interface IPreferenceService
    {
        Task<IEnumerable<PreferenceDto>> GetAllAsync(CancellationToken cancellationToken);

        Task<PreferenceDto> GetByIdAsync(Guid id, CancellationToken cancellationToken);

        Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken);

        Task<Guid> CreateAsync(CreatingPreferenceDto creatingPreferenceDto, CancellationToken cancellationToken);
    }
}
