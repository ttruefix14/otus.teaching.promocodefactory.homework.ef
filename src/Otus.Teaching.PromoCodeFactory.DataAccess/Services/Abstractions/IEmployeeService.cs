﻿using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Employee;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Abstractions
{
    public interface IEmployeeService
    {
        Task<IEnumerable<EmployeeDto>> GetAllAsync(CancellationToken cancellationToken);

        Task<EmployeeDto> GetByIdAsync(Guid id, CancellationToken cancellationToken);

        Task<bool> UpdateAsync(Guid id, UpdatingEmployeeDto updatingEmployeeDto, CancellationToken cancellationToken);

        Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken);

        Task<Guid> CreateAsync(CreatingEmployeeDto creatingEmployeeDto, CancellationToken cancellationToken);
    }
}
