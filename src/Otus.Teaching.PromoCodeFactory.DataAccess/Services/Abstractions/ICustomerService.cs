﻿using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Customer;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Abstractions
{
    public interface ICustomerService
    {
        Task<IEnumerable<CustomerDto>> GetAllAsync(CancellationToken cancellation);

        Task<CustomerDto> GetByIdAsync(Guid id, CancellationToken cancellationToken);

        Task<bool> UpdateAsync(Guid id, CreatingOrUpdatingCustomerDto updatingCustomerDto, CancellationToken cancellationToken);

        Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken);

        Task<Guid> CreateAsync(CreatingOrUpdatingCustomerDto creatingCustomerDto, CancellationToken cancellationToken);
    }
}
