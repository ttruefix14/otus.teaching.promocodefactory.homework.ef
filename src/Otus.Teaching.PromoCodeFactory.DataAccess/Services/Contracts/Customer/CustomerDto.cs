﻿using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Preference;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.PromoCode;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Customer
{
    public class CustomerDto
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public List<PromoCodeDto> PromoCodes { get; set; }

        public List<PreferenceDto> Preferences { get; set; }
    }
}
