﻿using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Preference;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Customer
{
    public class CreatingOrUpdatingCustomerDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public List<PassingPreferenceIdDto> Preferences { get; set; }
    }
}
