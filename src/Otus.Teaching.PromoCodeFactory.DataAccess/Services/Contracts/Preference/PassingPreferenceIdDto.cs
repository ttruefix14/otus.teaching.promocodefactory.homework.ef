﻿using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Preference
{
    public class PassingPreferenceIdDto
    {
        public Guid Id { get; set; }
    }
}
