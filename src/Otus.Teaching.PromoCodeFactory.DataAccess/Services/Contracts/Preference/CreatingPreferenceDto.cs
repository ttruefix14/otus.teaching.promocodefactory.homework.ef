﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Preference
{
    public class CreatingPreferenceDto
    {
        public string Name { get; set; }
    }
}
