﻿using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Customer;
using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Preference
{
    public class CustomerPreferenceDto
    {
        public Guid CustomerId { get; set; }

        public CustomerDto Customer { get; set; }

        public Guid PreferenceId { get; set; }

        public PreferenceDto Preference { get; set; }
    }
}
