﻿using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Preference
{
    public class PreferenceDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
