﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Role
{
    public class UpdatingRoleDto
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
