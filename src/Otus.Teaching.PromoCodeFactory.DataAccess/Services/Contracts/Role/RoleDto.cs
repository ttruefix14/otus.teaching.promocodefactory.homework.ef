﻿using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Role
{
    public class RoleDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
