﻿using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Role;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Employee
{
    public class UpdatingEmployeeDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public RoleDto Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}
