﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Employee
{
    public class CreatingEmployeeDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string RoleName { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}
