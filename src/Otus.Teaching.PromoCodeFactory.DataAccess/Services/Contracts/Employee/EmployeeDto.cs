﻿using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Role;
using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Employee
{
    public class EmployeeDto
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public RoleDto Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}