﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.PromoCode
{
    public class CreatingPromoCodeDto
    {
        public string ServiceInfo { get; set; }

        public string PartnerName { get; set; }

        public string PromoCode { get; set; }

        public string PreferenceName { get; set; }
    }
}
