﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Implementations
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IMapper _mapper;

        public EmployeeService(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository, IMapper mapper)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
            _mapper = mapper;
        }

        public async Task<Guid> CreateAsync(CreatingEmployeeDto creatingEmployeeDto, CancellationToken cancellationToken)
        {
            var roles =  _roleRepository.GetAll();
            var role = await roles.FirstOrDefaultAsync(r => r.Name == creatingEmployeeDto.RoleName, cancellationToken);

            var entity = _mapper.Map<CreatingEmployeeDto, Employee>(creatingEmployeeDto);

            if (role != null)
                entity.Role = role;
            else if (creatingEmployeeDto.RoleName != null)
                entity.Role = new Role() { Name = creatingEmployeeDto.RoleName };

            var guid = await _employeeRepository.CreateAsync(entity, cancellationToken);
            await _employeeRepository.SaveChangesAsync(cancellationToken);
            return guid;
        }

        public async Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken)
        {
            var result = await _employeeRepository.DeleteAsync(id, cancellationToken);
            await _employeeRepository.SaveChangesAsync(cancellationToken);
            return result;
        }

        public async Task<IEnumerable<EmployeeDto>> GetAllAsync(CancellationToken cancellationToken)
        {
            var employees = await _employeeRepository.GetAllAsync(cancellationToken);
            return _mapper.Map<IEnumerable<Employee>, IEnumerable<EmployeeDto>>(employees);
        }

        public async Task<EmployeeDto> GetByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            var employee = await _employeeRepository.GetByIdAsync(id, cancellationToken);
            return _mapper.Map<Employee, EmployeeDto>(employee);
        }

        public async Task<bool> UpdateAsync(Guid id, UpdatingEmployeeDto updatingEmployeeDto, CancellationToken cancellationToken)
        {
            var roles = _roleRepository.GetAll();
            var role = await roles.FirstOrDefaultAsync(r => r.Name == updatingEmployeeDto.Role.Name, cancellationToken);

            var entity = _mapper.Map<Employee>(updatingEmployeeDto);
            if (role != null)
            {
                role.Name = updatingEmployeeDto.Role?.Name;
                role.Description = updatingEmployeeDto.Role?.Description;
                await _roleRepository.UpdateAsync(role, cancellationToken);
                entity.Role = role;
            }

            entity.Id = id;
            var result = await _employeeRepository.UpdateAsync(entity, cancellationToken);
            await _employeeRepository.SaveChangesAsync(cancellationToken);
            return result;
        }
    }
}
