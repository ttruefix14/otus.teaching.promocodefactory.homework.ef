﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Role;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Implementations.Mapping
{
    public class RoleMappingsProfile : Profile
    {
        public RoleMappingsProfile()
        {
            CreateMap<Role, RoleDto>().ReverseMap();

            CreateMap<UpdatingRoleDto, Role>()
                .ForMember(d => d.Id, map => map.Ignore());
        }
    }
}
