﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Preference;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Implementations.Mapping
{
    public class PreferenceMappingsProfile : Profile
    {
        public PreferenceMappingsProfile()
        {
            CreateMap<Preference, PreferenceDto>().ReverseMap();

            CreateMap<CustomerPreference, PreferenceDto>()
                .ForMember(d => d.Id, map => map.MapFrom(src => src.PreferenceId))
                .ForMember(d => d.Name, map => map.MapFrom(src => src.Preference.Name));

            CreateMap<PassingPreferenceIdDto, CustomerPreference>()
                .ForMember(d => d.PreferenceId, map => map.MapFrom(src => src.Id))
                .ForMember(d => d.Preference, map => map.Ignore())
                .ForMember(d => d.CustomerId, map => map.Ignore())
                .ForMember(d => d.Customer, map => map.Ignore());

            CreateMap<CreatingPreferenceDto, Preference>()
                .ForMember(d => d.Id, map => map.Ignore());
        }
    }
}
