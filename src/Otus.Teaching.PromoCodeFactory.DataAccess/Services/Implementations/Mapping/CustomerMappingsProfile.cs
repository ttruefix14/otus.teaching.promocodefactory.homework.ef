﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Customer;


namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Implementations.Mapping
{
    public class CustomerMappingsProfile : Profile
    {
        public CustomerMappingsProfile()
        {
            CreateMap<Customer, CustomerDto>()
                .ForMember(d => d.Preferences, map => map.MapFrom(src => src.CustomerPreferences));

            CreateMap<CreatingOrUpdatingCustomerDto, Customer>()
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.FullName, map => map.Ignore())
                .ForMember(d => d.PromoCodes, map => map.Ignore())
                .ForMember(d => d.CustomerPreferences, map => map.MapFrom(src => src.Preferences));
        }
    }
}
