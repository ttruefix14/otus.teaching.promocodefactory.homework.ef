﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.PromoCode;
using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Implementations.Mapping
{
    public class PromoCodeMappingsProfile : Profile
    {
        public PromoCodeMappingsProfile()
        {
            CreateMap<PromoCode, PromoCodeDto>()
                .ForMember(d => d.Code, map => map.MapFrom(src => src.CodeText));

            CreateMap<CreatingPromoCodeDto, PromoCode>()
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.PartnerManager, map => map.Ignore())
                .ForMember(d => d.Customer, map => map.Ignore())
                .ForMember(d => d.Preference, map => map.Ignore())
                .ForMember(d => d.BeginDate, map => map.MapFrom(src => DateTime.Now))
                .ForMember(d => d.EndDate, map => map.MapFrom(src => DateTime.Now.AddYears(1)))
                .ForMember(d => d.CodeText, map => map.MapFrom(src => src.PromoCode));
        }
    }
}
