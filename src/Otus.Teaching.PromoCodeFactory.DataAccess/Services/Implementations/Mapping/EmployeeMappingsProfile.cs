﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Employee;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Implementations.Mapping
{
    public class EmployeeMappingsProfile : Profile
    {
        public EmployeeMappingsProfile()
        {
            CreateMap<Employee, EmployeeDto>();

            CreateMap<CreatingEmployeeDto, Employee>()
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.FullName, map => map.Ignore())
                .ForMember(d => d.RoleId, map => map.Ignore())
                .ForMember(d => d.Role, map => map.Ignore());

            CreateMap<UpdatingEmployeeDto, Employee>()
                .ForMember(d => d.Id, map => map.Ignore())
                .ForMember(d => d.FullName, map => map.Ignore())
                .ForMember(d => d.RoleId, map => map.Ignore());
        }
    }
}
