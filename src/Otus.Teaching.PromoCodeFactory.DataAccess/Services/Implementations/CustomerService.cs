﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Customer;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Implementations
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IMapper _mapper;

        public CustomerService(ICustomerRepository customerRepository, IMapper mapper)
        {
            _customerRepository = customerRepository;
            _mapper = mapper;
        }

        public async Task<Guid> CreateAsync(CreatingOrUpdatingCustomerDto creatingCustomerDto, CancellationToken cancellationToken)
        {
            var entity = _mapper.Map<Customer>(creatingCustomerDto);
            
            var guid = await _customerRepository.CreateAsync(entity, cancellationToken);
            await _customerRepository.SaveChangesAsync(cancellationToken);
            return guid;
        }

        public async Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken)
        {
            var result = await _customerRepository.DeleteAsync(id, cancellationToken);
            await _customerRepository.SaveChangesAsync(cancellationToken);
            return result;
        }

        public async Task<IEnumerable<CustomerDto>> GetAllAsync(CancellationToken cancellationToken)
        {
            var employees = await _customerRepository.GetAllAsync(cancellationToken);
            return _mapper.Map<IEnumerable<Customer>, IEnumerable<CustomerDto>>(employees);
        }

        public async Task<CustomerDto> GetByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            var entity = await _customerRepository.GetByIdAsync(id, cancellationToken);
            return _mapper.Map<CustomerDto>(entity);
        }

        public async Task<bool> UpdateAsync(Guid id, CreatingOrUpdatingCustomerDto updatingCustomerDto, CancellationToken cancellationToken)
        {
            var entity = _mapper.Map<Customer>(updatingCustomerDto);
            entity.Id = id;
            var result = await _customerRepository.UpdateAsync(entity, cancellationToken);
            
            await _customerRepository.SaveChangesAsync(cancellationToken);
            return result;
        }
    }
}
