﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.PromoCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Implementations
{
    public class PromoCodeService : IPromoCodeService
    {
        private readonly IMapper _mapper;
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public PromoCodeService(
            IMapper mapper, 
            IRepository<Preference> preferenceRepository, 
            IRepository<PromoCode> promoCodeRepository, 
            ICustomerRepository customerRepository, 
            IRepository<Employee> employeeRepository)
        {
            _mapper = mapper;
            _preferenceRepository = preferenceRepository;
            _promoCodeRepository = promoCodeRepository;
            _customerRepository = customerRepository;
            _employeeRepository = employeeRepository;
        }

        public async Task<IEnumerable<Guid>> CreateAsync(CreatingPromoCodeDto creatingPromoCodeDto, CancellationToken cancellationToken)
        {
            var customers = await _customerRepository.GetByPreferenceAsync(
                creatingPromoCodeDto.PreferenceName, cancellationToken);
            //так как рандомная сортировка по guid не работает в sqlite, использую в postgresql, чтоб не запрашивать всех сотрудников
            //var employees = _employeeRepository.GetAll(); 
            var employees = await _employeeRepository.GetAllAsync(cancellationToken);
            var preferences = _preferenceRepository.GetAll();


            var entity = _mapper.Map<PromoCode>(creatingPromoCodeDto);
            entity.PartnerManager = employees
                .Where(e => e.Role.Name == "PartnerManager")
                .OrderBy(e => Guid.NewGuid()).FirstOrDefault();
            entity.Preference = await preferences
                .Where(p => p.Name == creatingPromoCodeDto.PreferenceName)
                .FirstOrDefaultAsync(cancellationToken);

            var guids = new List<Guid>();
            Guid guid;
            foreach (var customer in customers)
            {
                entity.Customer = customer;
                entity.Id = Guid.NewGuid();
                guid = await _promoCodeRepository.CreateAsync(entity, cancellationToken);
                await _promoCodeRepository.SaveChangesAsync(cancellationToken);
                guids.Add(guid);
            }
            return guids;
        }

        public async Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken)
        {
            var result = await _promoCodeRepository.DeleteAsync(id, cancellationToken);
            await _promoCodeRepository.SaveChangesAsync(cancellationToken);
            return result;
        }

        public async Task<IEnumerable<PromoCodeDto>> GetAllAsync(CancellationToken cancellationToken)
        {
            return _mapper.Map<IEnumerable<PromoCodeDto>>(await _promoCodeRepository.GetAllAsync(cancellationToken));
        }
    }
}
