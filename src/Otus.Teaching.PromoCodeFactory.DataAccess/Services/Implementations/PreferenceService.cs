﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Abstractions;
using Otus.Teaching.PromoCodeFactory.DataAccess.Services.Contracts.Preference;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services.Implementations
{
    public class PreferenceService : IPreferenceService
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Preference> _preferenceRepository;

        public PreferenceService(IMapper mapper, IRepository<Preference> preferenceRepository)
        {
            _mapper = mapper;
            _preferenceRepository = preferenceRepository;
        }

        public async Task<Guid> CreateAsync(CreatingPreferenceDto creatingPreferenceDto, CancellationToken cancellationToken)
        {
            var result = await _preferenceRepository.CreateAsync(
                _mapper.Map<Preference>(creatingPreferenceDto), cancellationToken);
            await _preferenceRepository.SaveChangesAsync(cancellationToken);
            return result;
        }

        public async Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken)
        {
            var result = await _preferenceRepository.DeleteAsync(id, cancellationToken);
            await _preferenceRepository.SaveChangesAsync(cancellationToken);
            return result;
        }

        public async Task<IEnumerable<PreferenceDto>> GetAllAsync(CancellationToken cancellationToken)
        {
            return _mapper.Map<IEnumerable<Preference>, IEnumerable<PreferenceDto>>(
                await _preferenceRepository.GetAllAsync(cancellationToken));
        }

        public async Task<PreferenceDto> GetByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            return _mapper.Map<PreferenceDto>(await _preferenceRepository.GetByIdAsync(id, cancellationToken));
        }
    }
}
