﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly DbContext Context;
        private readonly DbSet<T> _entitySet;
        public EfRepository(DatabaseContext context)
        {
            Context = context;
            _entitySet = Context.Set<T>();
        }

        #region Get
        public virtual async Task<T> GetByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            return await _entitySet.FindAsync(new object[] { id }, cancellationToken);
        }

        #endregion

        #region GetAll
        public virtual IQueryable<T> GetAll(bool asNoTracking = false)
        {
            return asNoTracking ? _entitySet.AsNoTracking() : _entitySet;
        }

        public async Task<IEnumerable<T>> GetAllAsync(CancellationToken cancellationToken, bool asNoTracking = false)
        {
            return await GetAll().ToListAsync(cancellationToken);
        }

        #endregion

        #region Create
        public virtual async Task<Guid> CreateAsync(T entity, CancellationToken cancellationToken)
        {
            var result = await _entitySet.AddAsync(entity, cancellationToken);
            return result.Entity.Id;
        }

        #endregion

        #region Update

        public virtual async Task<bool> UpdateAsync(T updatedEntity, CancellationToken cancellationToken)
        {
            ArgumentNullException.ThrowIfNull(updatedEntity);
            _entitySet.Update(updatedEntity);
            return true;

        }

        #endregion

        #region Delete

        public virtual async Task<bool> DeleteAsync(Guid id, CancellationToken cancellationToken)
        {
            var entity = await _entitySet.FindAsync(new object[] { id }, cancellationToken);

            if (entity == null)
                throw new Exception($"No object with id: {id} in database");
           
            _entitySet.Remove(entity);
            return true;
        }

        public virtual async Task SaveChangesAsync(CancellationToken cancellationToken)
        {
            await Context.SaveChangesAsync(cancellationToken);
        }
        #endregion
    }
}
