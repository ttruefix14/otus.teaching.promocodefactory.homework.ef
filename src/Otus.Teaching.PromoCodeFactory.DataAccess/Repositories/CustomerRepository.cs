﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository : EfRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(DatabaseContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Customer>> GetByPreferenceAsync(string PreferenceName, CancellationToken cancellationToken)
        {
            var entities = GetAll()
                .Where(c => c.CustomerPreferences.Select(cp => cp.Preference.Name).Contains(PreferenceName));

            return await entities.ToListAsync<Customer>(cancellationToken);
        }

        new public async Task<bool> UpdateAsync(Customer updatedEntity, CancellationToken cancellationToken)
        {
            ArgumentNullException.ThrowIfNull(updatedEntity);

            var entity = await GetByIdAsync(updatedEntity.Id, cancellationToken);
            if (entity == null)
            {
                throw new Exception($"No object with id: {updatedEntity.Id} in database");
            }

            entity.CustomerPreferences.Clear();
            foreach (CustomerPreference customerPreference in updatedEntity.CustomerPreferences)
            {
                entity.CustomerPreferences.Add(customerPreference);
            }
            Context.Set<Customer>().Update(entity);
            return true;
        }
    }
}
