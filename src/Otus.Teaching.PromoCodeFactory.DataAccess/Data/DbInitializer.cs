﻿using Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DbInitializer : IDbInitializer
    {
        private readonly DatabaseContext _dataContext;

        public DbInitializer(DatabaseContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            _dataContext.AddRange(FakeDataFactory.Preferences);
            _dataContext.SaveChanges();

            _dataContext.AddRange(FakeDataFactory.Customers);
            _dataContext.SaveChanges();

            _dataContext.AddRange(FakeDataFactory.Roles);
            _dataContext.SaveChanges();

            _dataContext.AddRange(FakeDataFactory.Employees);
            _dataContext.SaveChanges();

        }

    }
}
